
# Curated Curaprofiles


This repository stores curaprofiles that wOrK On mY mAChinEs!
They might be helpful to other people as a starting point to get good results quickly.

## Disclaimer

>   Remember to update the printspeeds to match the flowrate your hotend is capable of.
    Also keep in mind that some higher temperature profiles like ABS reach temperatures that exeed the safe
    operation temperatures of PTFE guided heatbrakes and require hotents featuring full metal heatbrakes.
    My bed material of choice is FR4 if yours is not, these settings might not translate to your machine.


Across all profiles / materials in common is printing the first layer almost ridiculously hot.
That way the extruded filament is very liquid and sticks strongly to the buildplate.


---

## Profiles


### abs_200um_max_3h
    
Prints ABS without any additional printbed adhesion and without warping for up to 3h.
At > 3h I experience warping, which is probably due to the air in my enclosure not being hot enough.

